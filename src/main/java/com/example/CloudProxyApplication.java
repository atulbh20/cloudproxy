package com.example;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.hateoas.Resource;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;



@EnableZuulProxy
@EnableBinding(Source.class)
@EnableDiscoveryClient
@SpringBootApplication
public class CloudProxyApplication {

	public static void main(String[] args) {
		SpringApplication.run(CloudProxyApplication.class, args);
	}

	@RestController
	@RequestMapping("/persons")
	class ApiGateWayRestController{
		
		@LoadBalanced
		private RestTemplate restTemplate;
		
		@Output(Source.OUTPUT)
		@Autowired
		private MessageChannel messageChannel;
		
		@RequestMapping(method = RequestMethod.POST)
		public void write(@RequestBody Person s){
			System.out.println("-------------------------------------------------------------------------------");
			System.out.println(s.toString());
			System.out.println("-------------------------------------------------------------------------------");
			this.messageChannel.send(MessageBuilder.withPayload(s.getName()).build());
		}
		
		@RequestMapping("/names")
		public Collection<String> getPersonNames() {
			ParameterizedTypeReference<Resource<Person>> ptr = new ParameterizedTypeReference<Resource<Person>>() {};
			ResponseEntity<Resource<Person>> responseEntity = 
					this.restTemplate.exchange("http:///person-service/persons", HttpMethod.GET, null, ptr);
			
			List<Person> lst = (List<Person>) responseEntity.getBody().getContent();
			Collection<String> strs = null;
			
			for(Person p: lst){
				strs.add(p.getName());
			}
			
			return strs;
		}
	}
}

class Person{
	private Long id;
	private String name;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("Person={");
		sb.append("id=").append(id);
		sb.append(", name=").append(name);
		sb.append("}");
		return sb.toString();
	}
}